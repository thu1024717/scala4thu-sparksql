import org.apache.spark.SparkConf
import org.apache.spark.sql.{DataFrame, SparkSession}

/**
  * Created by mark on 04/06/2017.
  */
object YourFirstSparkSQLApp extends App{

  val ss = SparkSession
    .builder()
    .master("local[*]")
    .appName("Spark SQL first example")
    .getOrCreate()

  val src: DataFrame =ss.read.option("header", "true")
    .csv("dataset/small-ratings.csv")

  src.createOrReplaceTempView("ratings")

  //ss.sql("SELECT * FROM ratings WHERE movieId=30707 AND userId=107799").show()


//ex.5
  val moviesrc: DataFrame =ss.read.option("header", "true")
    .csv("dataset/movies.csv")

  moviesrc.createOrReplaceTempView("movies")

  //ss.sql("SELECT movieId,title FROM movies WHERE movieId=10").show()

  ss.sql("SELECT m.movieId,m.title,rating,timestamp\nFROM ratings AS r JOIN movies AS m WHERE r.movieId=m.movieId\nORDER BY timestamp DESC").show()
}